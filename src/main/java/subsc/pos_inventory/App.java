package subsc.pos_inventory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import subsc.pos_inventory.connectPos_views.ConnectPos_Views;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(ConnectPos_Views.POS_AdminLogin));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}

//https://www.youtube.com/watch?v=voo1UVDOB-I&t=19s
