/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package subsc.pos_inventory.Connectdb;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ConnectDatabase {

    private static ConnectDatabase instance = null;

    public ConnectDatabase() {

    }

    public static ConnectDatabase INSTANCE() {
        if (instance == null) {
            instance = new ConnectDatabase();
        }
        return instance;
    }

    public Connection connect() {
        try {
            class Pos_Connection {

                private Pos_Connection Con = null;
                private String ConnectUrl = "jdbc:mysql://localhost:3306/pos_inventory?zeroDateTimeBehavior=CONVERT_TO_NULL";
                public Connection conn = null;

                private Pos_Connection() {
                    try {
                        try {
                            Class.forName("com.mysql.cj.jdbc.Driver");
                            conn = DriverManager.getConnection(ConnectUrl, "root", "");

                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(ConnectDatabase.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } catch (SQLException e) {
                    }

                }

            }
            return new Pos_Connection().conn;
        } catch (Exception e) {
        }
        return null;
    }

}
