module subsc.pos_inventory {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;
    
    requires java.sql;
    opens subsc.pos_inventory to javafx.fxml;
    exports subsc.pos_inventory;
    opens subsc.pos_inventory.controllers;
    exports subsc.pos_inventory.controllers;
}
